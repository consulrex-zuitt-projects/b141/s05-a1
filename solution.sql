SELECT * FROM customers WHERE country = "Philippines";

SELECT contactLastName, contactFirstName FROM customers
WHERE customerName = "La Rochelle Gifts";

SELECT productName, MSRP FROM products
WHERE productName = "The Titanic";

SELECT lastName, firstName FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

SELECT customerName FROM customers
WHERE state IS NULL;

SELECT firstName, lastName, email FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve";

SELECT customerName, country, creditLimit FROM customers
WHERE country != "USA"
AND creditLimit > 3000;

SELECT customerName FROM customers
WHERE customerName
NOT LIKE "%a%";

SELECT customerNumber FROM orders
WHERE comments
LIKE "%DHL%";

SELECT productLine from productlines
WHERE textDescription
LIKE "%state of the art%";

SELECT DISTINCT country FROM customers;

SELECT DISTINCT status FROM orders;

SELECT customerName, country FROM customers
WHERE country
IN ("USA", "France", "Canada");

SELECT firstName, lastName, officeCode FROM employees
WHERE officeCode = 5;

SELECT customerName FROM customers
WHERE salesRepEmployeeNumber = 1166;

SELECT products.productName, customers.customerName
FROM customers 
JOIN orders ON
customers.customerNumber = orders.customerNumber
JOIN orderdetails ON
orders.orderNumber = orderdetails.orderNumber
JOIN products ON
orderdetails.productCode = products.productCode
WHERE customers.customerName = "Baane Mini Imports";

SELECT employees.firstName, 
employees.lastName, 
customers.customerName, 
offices.country 
FROM offices JOIN employees ON
offices.officeCode = employees.officeCode
JOIN customers ON
employees.employeeNumber = customers.salesRepEmployeeNumber
WHERE customers.country = offices.country;

SELECT lastName, firstName FROM employees
WHERE reportsTo = 1143;

SELECT productName, MSRP FROM products
ORDER BY MSRP DESC LIMIT 1;

SELECT COUNT(*) FROM customers
WHERE country = "UK";

SELECT productLine, COUNT(*) FROM products
GROUP BY productLine;

SELECT salesRepEmployeeNumber, COUNT(*) FROM customers
GROUP BY salesRepEmployeeNumber;

SELECT productName, quantityInStock FROM products
WHERE productLine = "Planes" AND
quantityInStock < 1000;
